import csv

import numpy as np
import matplotlib.pyplot as plt



def plot_histogram_pickups_dropoffs(start, finish):
    x = [i for i in range(50)]
    y_start = []
    y_fin = []
    for i in range(50):
       y_start.append(start.count(i))
       y_fin.append(finish.count(i))


    # create plot
    plt.figure(3)
    index = np.arange(len(y_start))
    bar_width = 0.20
    opacity = 1

    rects1 = plt.bar(index, y_start, bar_width,
                     alpha=opacity,
                     color='b',
                     label='Pickup')

    rects2 = plt.bar(index + bar_width, y_fin, bar_width,
                     alpha=opacity,
                     color='r',
                     label='Dropoff')

    plt.xlabel('Node Location')
    plt.ylabel('Frequency')
    plt.title('Histogram of Pickup/Dropoff Locations')
    #plt.xticks(index + bar_width)
    plt.legend()
    #plt.tight_layout()
    plt.show()



def plot_wait_vs_drivers(times,start,finish):
    x =[]
    y = []
    for i in range(1,10):
        x.append(i)
        y.append(sum(waiting(times,start,finish,i)))

    plt.figure(1)

    for k in range(len(x)):
       plt.text(x[k], y[k], '(' + str(x[k]) +',' +str(y[k]) +')')



    plt.title("Wait Times vs Number of Drivers")
    plt.xlabel("Number of Drivers")
    plt.ylabel("Wait Times ")
    plt.plot(x,y,'r-o')

def plot_wait_vs_drivers_large(times,start,finish):
    x =[]
    y = []

    for i in range (10,21):
        x.append(i)
        y.append(sum(waiting(times, start, finish, i)))
    x.append(50)
    y.append(sum(waiting(times, start, finish, 50)))
    plt.figure(4)

    for k in range(len(x)):
       plt.text(x[k], y[k], '(' + str(x[k]) +',' +str(y[k]) +')')



    plt.title("Wait Times vs Number of Drivers")
    plt.xlabel("Number of Drivers")
    plt.ylabel("Wait Times ")
    plt.plot(x,y,'r-o')

def plot_wait_two_drivers(times,start,finish):

    x = []
    plt.figure(2)
    plt.title("Wait Times with 2 Drivers")
    plt.xlabel("Request Number")
    plt.ylabel("Wait Time")

    data = waiting(times, start, finish, 2)

    for i in range(len(data)):
        x.append(i)

    #plt.text(6.5, 250, 'Total Wait Time ' + str(sum(data)))
    plt.plot(x,data, 'bo',)


def floyd_walsh():
    global graph
    inf = float("inf")

   #initialize solution matrix with adjacency matrix
    dist = [[graph[i][j] for i in range(len(graph))] for j in range(len(graph))]


    for k in range(len(graph)):
        for i in range(len(graph)):
            for j in range(len(graph)):
                if(i!=j  and dist[i][j]== 0 ):
                    dist[i][j] = inf
                dist[i][j] = min(dist[i][j], dist[i][k]+dist[k][j])


    return dist


def min_dist(dist,spv):
    min= float('inf')

    for v in range(len(graph)):
        if spv[v] == 0 and dist[v]< min:
            spv[v] ==1
            min = dist[v]
            min_index = v
    return min_index


def dijsktra_alg(source):

    global graph
    # initialize the distance array with infinity
    dist = [ float("inf") for i in range(len(graph))]
    # spv = shortest path vertex basically the set of vertexes in the shortest path
    # vertex is 0 if not in path, 1 if vertex is in path
    spv = [0 for i in range(len(graph))]
    dist[source] = 0

    for i in range(len(graph)-1):

        u = min_dist(dist,spv)
        spv[u] = 1

        for v in range(len(graph)):

            if spv[v] == 0 and graph[u][v] and dist[u] != (float('inf')) and dist[u]+graph[u][v] < dist[v]:
                dist[v] = dist[u] + graph[u][v]
    return dist


def waiting(time, start, finish,num_drivers):
    global graph
    shortest_path = floyd_walsh()
    #shortest_path = [dijsktra_alg(i) for i in range(len(graph))]
    wait = []
    min = float("inf")

    #find the most connnected node in the graph to start the drivers
    for i in range(len(graph)):
        tmp = sum(shortest_path[i])
        if tmp < min:
            min = tmp
            most_connected =i

    #initialze driver array
    # driver[i][0] holds the position, driver[i][1] holds the time available,
    driver = []
    for i in range(num_drivers):
        pos_time = [0 for j in range(2)]
        pos_time[0] = most_connected
        pos_time[1] = time[0]
        driver.append(pos_time)


    for i in range(len(time)):

        min_time = float('inf')

        #Determine the closest driver to the Customer
        for j in range(num_drivers):
            if driver[j][1] <= time[i]:
                driver[j][1] = time[i]

            closest = shortest_path[driver[j][0]][start[i]] + driver[j][1]

            if min_time > closest:
                min_time = closest
                pickup_time = closest
                driver_index = j

        #Add to the wait times
        if pickup_time > time[i]:
            wait.append(pickup_time-time[i])
        else:
            wait.append(0)

        #Update Driver position and time finished
        driver[driver_index][0] = finish[i]
        driver[driver_index][1] = pickup_time + shortest_path[start[i]][finish[i]]


    return wait




###############################################
#setup
def read_csv(name,array):

     with open (name,'r') as csv_file:
         csv_reader = csv.reader(csv_file)
         for row in csv_reader:
             array.append(row)
     csv_file.close()


graph = []
read_csv('network.csv',graph)
graph = np.array(graph).astype(int)
requests = []
requests_2 = []
read_csv('requests.csv',requests)
requests = np.array(requests).astype(int)
read_csv('supplementpickups.csv',requests_2)
print(requests_2)
requests_2 = np.array(requests_2).astype(int)
######################################################
#main

# break up into 1 dimension
times =[i[0] for i in requests]
# subract one from the values to make sure indexing lines up with the network indexing

start= [(i[1]-1) for i in requests]
finish =[(i[2]-1) for i in requests]

#supplemental testing data
times_2 =[i[0] for i in requests_2]
start_2= [(i[1]-1) for i in requests_2]
finish_2 =[(i[2]-1) for i in requests_2]


# total = waiting(times,start,finish,2)
# print(sum(total))
#plot_wait_vs_drivers(times,start,finish)
plot_wait_vs_drivers(times_2,start_2,finish_2)
#plot_wait_vs_drivers_large(times,start,finish)
plot_wait_vs_drivers_large(times_2,start_2,finish_2)
#plot_wait_two_drivers(times,start,finish)
plot_wait_two_drivers(times_2,start_2,finish_2)
#plot_histogram_pickups_dropoffs(start,finish)
plot_histogram_pickups_dropoffs(start_2,finish_2)

print("Total wait time with 2 Drivers using the 2nd data set")
print(sum(waiting(times_2,start_2,finish_2,2)))

print("Wait time for 1000 drivers")
print(sum(waiting(times,start,finish,1000)))

print("Wait time for 10000 drivers")
print(sum(waiting(times,start,finish,10000)))



plt.show()

