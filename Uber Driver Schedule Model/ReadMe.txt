Example of finding the minimum wait times for an uber service. THe network is an adjacency matrix that represents weights
from one node to another. For example the time to go from node i to j would be network[i][j] = time. Requests are in the format
col1 = time of request, col2 = start location, col3 =  end location.