//
// Created by Chris on 2018-11-15.
//

#include <string>
#include <iostream>
#include <ctime>
#include "jumblepuzzle_1.h"


typedef char* charArrayPtr;
using namespace std;
int JumblePuzzle::getRowPos() const {
    return row_pos;
}

int JumblePuzzle::getColPos() const {
    return col_pos;
}

char JumblePuzzle::getDirection() const {
    return direction;
}

int JumblePuzzle::getSize() const {
    return size;
}
string JumblePuzzle::getDifficulty() const {
    return diff;
}
string JumblePuzzle::getHide() const {
    return hide;
}
charArrayPtr* JumblePuzzle:: getJumble() const{
    charArrayPtr* new_jp = new charArrayPtr[size];
    for(int i =0 ; i<size; i++){
        new_jp [i] = new char[size];
        for (int j=0 ; j<size;j++){
            new_jp[i][j] = jp[i][j];

        }
    }
    return new_jp;
}
JumblePuzzle::JumblePuzzle(){
    diff = "";
    hide = "";
    jp = nullptr; //jumble puzzle
    row_pos = 0;
    col_pos = 0;
    direction = ' ';
    size = 0;

}
int JumblePuzzle::puzzleSize(){
    int tmp = 0;
    if (diff == "easy") {
        tmp = 2;
    }else if (diff == "medium") {
        tmp = 3;
    }else if (diff == "hard") {
        tmp = 4;
    }else{
        throw BadJumbleException("Difficulty is incorrect please choose from: easy,medium,hard");
    }
    return tmp*hide.length();

}

bool JumblePuzzle:: hideWord() {

    bool work = false;
    int j = 0;
    int wordLen = hide.length();
     if (direction == 'e') {
        if ((col_pos + wordLen) >= size) {
            return false;
        } else {
            for (int i = col_pos; i < col_pos + wordLen; i++) {
                jp[row_pos][i] = hide.at(j);
                j++;
            }
            return true;
        }
    }else if (direction == 'w') {
        if ((col_pos - wordLen) < -1) {
            return false;
        } else {
            for (int i = col_pos; i > col_pos - wordLen; i--) {
                jp[row_pos][i] = hide.at(j);
                j++;
            }
            return true;
        }
    } else if (direction == 'n') {
        if ((row_pos - wordLen) < -1) {
            return false;
        } else {
            for (int i = row_pos; i > row_pos - wordLen; i--) {
                jp[i][col_pos] = hide.at(j);
                j++;
            }
            return true;
        }
    }else if (direction == 's') {
        if ((row_pos + wordLen) >= size) {
            return false;
        } else {
            for (int i = row_pos; i < row_pos + wordLen; i++) {
                jp[i][col_pos] = hide.at(j);
                j++;
            }
            return true;
        }
    }
    return work;

}

char JumblePuzzle::randDir(){
    int index = rand() % 4;
    string dir = "nswe";
    return dir.at(index);

}

char JumblePuzzle::randChar() {
    int index = rand() % 26;
    string a_to_z = "abcdefghijklmnopqrstuvwxyz";
    return a_to_z.at(index);

}
JumblePuzzle::JumblePuzzle(const string &word, const string &difficulty) {
    diff = difficulty;
    hide = word;

    int len = hide.length();
    if (len < 3) {
        throw BadJumbleException("Word to hide is too small");
    } else if (len > 10) {
        throw BadJumbleException("Word to hide is too long");
    }

    size = puzzleSize();
    jp = new charArrayPtr[size];

    for (int i = 0; i < size; i++) {
        jp[i] = new char[size];
        for (int j = 0; j < size; j++) {
            jp[i][j] = randChar();
        }
    }


    row_pos = rand() % size;
    col_pos = rand() % size;
    bool placed = false;

    while (placed == false) {

        direction = randDir();
        placed = hideWord();

    }

}

JumblePuzzle::JumblePuzzle(const JumblePuzzle& copy) {
    diff = copy.getDifficulty();
    hide = copy.getHide();
    size = copy.getSize();

    if (copy.getJumble()) {
    jp = new charArrayPtr[size];
    for (int i = 0; i < size; i++) {
        jp[i] = new char[size];
        for (int j = 0; j < size; j++) {
            jp[i][j] = copy.getJumble()[i][j];

        }

    }
    }
}
JumblePuzzle::~JumblePuzzle() {
    for (int i = 0; i<size;i++){
        delete[] jp[i];
        jp[i] = nullptr;
    }
    delete[] jp;
    jp = nullptr;

}
JumblePuzzle& JumblePuzzle::operator=(const JumblePuzzle &r) {
    if(this != &r){
        for (int i = 0; i <size ; i++) {
            delete[] jp[i];
        }
        delete[] jp;
        diff = r.getDifficulty();
        hide = r.getHide();
        size = r.getSize();
        r.getJumble();
        jp = new char*[size];
        for (int i; i<size; i++){
            jp[i] = new char[size];
            for(int j =0 ; j<size;j++){
                jp[i][j] = r.getJumble()[i][j];
            }
        }
        row_pos = r.getRowPos();
        col_pos = r.getColPos();
        direction = r.getDirection();

    }
    return *this;
}

BadJumbleException::BadJumbleException(const string &e) {
    error_msg = e;
}
string BadJumbleException::what()const{
    return error_msg;
}

