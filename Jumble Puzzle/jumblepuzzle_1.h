//
// Created by Chris on 2018-11-15.
//

#ifndef JUMBLEPUZZLE_JUMBLEPUZZLE_1_H
#define JUMBLEPUZZLE_JUMBLEPUZZLE_1_H
#include <string>
#include <iostream>
#include <ctime>

using namespace std;
typedef char *charArrayPtr;

class JumblePuzzle {
public:
//Construtors
    JumblePuzzle();

    JumblePuzzle(const string& word, const string& difficulty);

//Big Three
//copy constructor
    JumblePuzzle(const JumblePuzzle &copy);

//destructor
    ~JumblePuzzle();

//equals operator
    JumblePuzzle &operator=(const JumblePuzzle &r);

    //Accsessors
    int getColPos()const; //gets start column
    int getRowPos()const; //gets start row
    char getDirection()const; //gets direction
    string getDifficulty()const; //gets difficulty
    string getHide()const; //gets word to hide
    int getSize()const; //gets size of puzzle
    charArrayPtr* getJumble() const; //returns pointer to a jumble puzzle

private:
    //helper functions for construtor
    int puzzleSize();//generates puzzle size based off of difficulty
    bool hideWord();//hides the word
    char randChar();//generates random char for the puzzle
    char randDir();//generates a random direction to place the word

    charArrayPtr* jp; //jumble puzzle
    int row_pos = 0;//row of the start of the hidden word
    int col_pos = 0;//col of the start of the hidden word
    char direction; // direction of the hidden word(north south east west
    int size; // size of the word
    string diff ; //difficulty of the puzzle easy, med or hard
    string hide ; // string to hide

};
//Exception Class supply a error message to the constructor when you throw an error
class BadJumbleException{
public:
    BadJumbleException(const string& e);
    string what() const;
private:
    string error_msg;


};

#endif //JUMBLEPUZZLE_JUMBLEPUZZLE_1_H
