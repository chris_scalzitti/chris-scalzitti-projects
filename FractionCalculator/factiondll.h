#ifndef FACTIONDLL_H
#define FACTIONDLL_H

#include "factiondll_global.h"
#include <iostream>

using namespace std;


class FACTIONDLLSHARED_EXPORT Fraction
{

    //Class to handle fractions <numerator>/<denominator> where numerator and denominator are integers

    public:
        //Constructor that creates a fraction of 0/1
        Fraction();

        //Constructor that creates a fraction of n/1
        Fraction(int n);

        //Constructor that creates a fraction of num/den
        Fraction(int num, int den);

        //Accessor returns numerator
        int numerator() const;

        //Accessor returns denominator
        int denominator() const;

        // Helper function that returns the integer difference between two fractions .
        int compare(const Fraction &right) const;

        //Overloading Increment Operators
        // Increments fraction by 1
        Fraction &operator++();//post increment

        Fraction operator++(int unused);//pre increment

        //Adds the value of a fraction updating original fraction
        Fraction &operator+=(Fraction &right);

    private:
        //Declaring members of class
        // top is numerator bottom is denominator
        int top, bottom;

        //Helper functions that calculates the greatest common divisor of 2 numbers
        int gcd(int n, int m);

    };

    //Overload Arithmetic Operators
   FACTIONDLLSHARED_EXPORT Fraction operator+(const Fraction &left, const Fraction &right);

   FACTIONDLLSHARED_EXPORT  Fraction operator-(const Fraction &left, const Fraction &right);

    FACTIONDLLSHARED_EXPORT Fraction operator*(const Fraction &left, const Fraction &right);

    FACTIONDLLSHARED_EXPORT Fraction operator/(const Fraction &left, const Fraction &right);

    FACTIONDLLSHARED_EXPORT Fraction operator-(const Fraction &value);

    //Overload Boolean Comparator Operators
    bool operator>(const Fraction &left, const Fraction &right);

    bool operator<(const Fraction &left, const Fraction &right);

    bool operator>=(const Fraction &left, const Fraction &right);

    bool operator<=(const Fraction &left, const Fraction &right);

    bool operator==(const Fraction &left, const Fraction &right);

    bool operator!=(const Fraction &left, const Fraction &right);


    //Overload Stream Operators
    ostream &operator<<(ostream &out, const Fraction &val);

    istream &operator>>(istream &in, Fraction &val);


class FractionException {
 public:
     const char* what()const;
 };

#endif // FACTIONDLL_H
