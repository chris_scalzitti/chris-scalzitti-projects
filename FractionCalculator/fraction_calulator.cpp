//
// Created by Chris Scalzitti    on 2018-10-10.
//
#include <iostream>

using namespace std;

#include "fraction_14cs76.h"

//An exception is thrown when a fraction with a 0 value denominator is attempted to be created
const char *FractionException::what() const {
    return "Cannot divide by zero";
}

//Helper function that calculates the greatest common divisor of 2 numbers
int Fraction::gcd(int n, int m) {
    if (m <= n && n % m == 0) {
        return m;
    } else if (n < m) {
        return gcd(m, n);
    } else {
        return gcd(m, (n % m));
    }
}

//Base Constructor
Fraction::Fraction() {
    top = 0;
    bottom - 1;
}

//Constructor 1 integer inputs
Fraction::Fraction(int n) {
    top = n;
    bottom = 1;
}

//Contructor 2 integer inputs
Fraction::Fraction(int num, int den) {
    int sign = 1;
    int gcd_num;

    if (den == 0) {
        throw FractionException();
    } else if (den < 0 || (num < 0 && den < 0)) {
        sign = -1;
    }
    gcd_num = gcd(abs(num), abs(den));
    top = num / gcd_num * sign;
    bottom = den / gcd_num * sign;
}

//Accessor Functions
int Fraction::numerator() const { return top; };

int Fraction::denominator() const { return bottom; };

//Member Operator Overloading
//Post increment
Fraction &Fraction::operator++() {
    top += bottom;
    return *this;
};

//Pre Increment
Fraction Fraction::operator++(int unused) {
    const Fraction old(*this);
    this->top += bottom;
    return old;

}

Fraction &Fraction::operator+=(Fraction &right) {
    top = top * right.denominator() + right.numerator() * bottom;
    bottom = bottom * right.denominator();

    return *this;
}


//Overload Binary Arethmetic Operators
Fraction operator+(const Fraction &left, const Fraction &right) {
    Fraction result(left.numerator() * right.denominator()
                    + right.numerator() * left.denominator(),
                    left.denominator() * right.denominator());
    return result;
}

Fraction operator-(const Fraction &left, const Fraction &right) {
    Fraction result(left.numerator() * right.denominator()
                    - right.numerator() * left.denominator(),
                    left.denominator() * right.denominator());
    return result;
}

Fraction operator*(const Fraction &left, const Fraction &right) {
    Fraction result(left.numerator() * right.numerator(),
                    left.denominator() * right.denominator());
    return result;
}

Fraction operator/(const Fraction &left, const Fraction &right) {
    Fraction result(left.numerator() * right.denominator(),
                    left.denominator() * right.numerator());
    return result;
}

Fraction operator-(const Fraction &val) {
    Fraction result(-1 * val.numerator(), val.denominator());
    return result;
}

//Comparator Function
int Fraction::compare(const Fraction &right) const {
    // Return the numerator of the difference
    return top * right.denominator() - bottom * right.numerator();
}

//Overload Boolean Operators
bool operator>(const Fraction &left, const Fraction &right) {
    return (left.compare(right) > 0);
}

bool operator>=(const Fraction &left, const Fraction &right) {
    return (left.compare(right) >= 0);
}

bool operator<(const Fraction &left, const Fraction &right) {
    return (left.compare(right) < 0);
}

bool operator<=(const Fraction &left, const Fraction &right) {
    return (left.compare(right) <= 0);
}

bool operator==(const Fraction &left, const Fraction &right) {
    return (left.compare(right) == 0);
}

bool operator!=(const Fraction &left, const Fraction &right) {
    return (left.compare(right) != 0);
}

//Stream operator overload
ostream &operator<<(ostream &out, const Fraction &value) {
    out << value.numerator() << "/" << value.denominator();
    return out;
}

istream &operator>>(istream &in, Fraction &val) {
    int top, bottom;
    char slash;
    in >> top;
//if Enter key pressed
    if (in.peek() != 10) {
        in >> slash;
        if (in.peek() == '0') {
            //Dont add any to the sum
            top = 0;
            throw FractionException();
        } else {
            in >> bottom;
            val = Fraction(top, bottom);
        }
    } else {
        val = Fraction(top);
    }
    return in;
}
