#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include"factiondll.h"
#include "factiondll_global.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void error_den_0(int val);
    void keyPressEvent(QKeyEvent* event);


private slots:
    void on_den1_textEdited(const QString &arg1);

    void on_den2_textEdited(const QString &arg1);

    void on_num1_textEdited(const QString &arg1);

    void on_num2_textEdited(const QString &arg1);

    void on_Equals_clicked();

    void clear_result();
private:
    Ui::MainWindow *ui;
   int num1=1;
   int num2=1;
   int den1=1;
   int den2=1;
   int edited_num1 =0;
   int edited_num2 =0;
   int edited_den1 =0;
   int edited_den2 =0;



   };
 //Fraction operator+(const Fraction &left, const Fraction &right);

#endif // MAINWINDOW_H
