#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "factiondll.h"
#include "factiondll_global.h"
#include <QDebug>
#include <QKeyEvent>
#include<QKeySequence>
#include<QString>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->comboBox->addItems({"Multiply","Divide","Add","Subtract"});
    QValidator *validate = new QIntValidator(-999999, 9999999, this);

    ui->num1->setValidator(validate);
   ui->num2->setValidator(validate);
   ui->den1->setValidator(validate);
   ui->den2->setValidator(validate);

   ui->num1->setMaxLength(6);
    ui->num2->setMaxLength(6);
    ui->den1->setMaxLength(6);
    ui->den2->setMaxLength(6);
    ui->num_result->setReadOnly(true);
    ui->den_result->setReadOnly(true);






}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::clear_result(){
      ui->num_result->setText("");
      ui->den_result->setText("");
}


void MainWindow::on_den1_textEdited(const QString &arg1)
{
    edited_den1=1;
    den1 = arg1.toInt();
    error_den_0(den1);
    clear_result();

    qDebug()<<"den 1 = "<<den1;
 }

void MainWindow::on_den2_textEdited(const QString &arg1)
{

    edited_den2=1;
    den2=arg1.toInt();
    error_den_0(den2);
    clear_result();
    qDebug()<<"den 2 = "<<den2;

}
void MainWindow::on_num1_textEdited(const QString &arg1)
{

    edited_num1=1;
    num1=arg1.toInt();
    clear_result();
    qDebug()<<"num1 = "<<num1;

}
void MainWindow::on_num2_textEdited(const QString &arg1)
{
    edited_num2=1;
    num2=arg1.toInt();
    clear_result();
    qDebug()<<"num2  = "<<num2;

}

void MainWindow::error_den_0(int val){
    if(val == 0 ){
         ui->ErrorMsg->setText("Fraction Denominator Cannot Be Zero");
    }
    else {
        ui->ErrorMsg->setText("");
    }
  }
void MainWindow::keyPressEvent(QKeyEvent* event)
{
    if(event->key() == Qt::Key_Enter|| event->key() == Qt::Key_Return){
       on_Equals_clicked();

    }

    if (event->modifiers() & Qt::AltModifier and event->key() == Qt::Key_C){
        this ->close();

     }
}


void MainWindow::on_Equals_clicked()
{

    if (edited_den1 and edited_den2 and edited_num1 and edited_num2){

         ui->ErrorMsg->setText("");

    if (den1 !=0 and den2 !=0){

    Fraction in1(num1,den1);
    Fraction in2(num2,den2);
      qDebug()<<"in1  = "<<in1.numerator()<<in1.denominator();
      qDebug()<<"in2  = "<<in2.numerator()<<in2.denominator();
    Fraction res;


    if (ui->comboBox->currentText() == "Multiply"){
       res = in1 *in2;
       qDebug()<<in1.numerator()<<in2.numerator();

    }
    else if (ui->comboBox->currentText() == "Divide"){
        res = in1/in2;
    }
    else if (ui->comboBox->currentText() == "Add"){
        res = in1 + in2;
    }
    else if (ui->comboBox->currentText() == "Subtract"){
         res = in1 - in2;
    }
    ui->num_result->setText(QString::number(res.numerator()));
    ui->den_result->setText(QString::number(res.denominator()));
    }

    }else {
        ui->ErrorMsg->setText("Please enter values for the fractions");

    }
}

