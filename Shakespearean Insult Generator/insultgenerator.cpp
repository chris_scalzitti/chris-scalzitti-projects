/*
 * insultgenerator_14cs76.cpp
 *
 *  Created on: Sep 10, 2018
 *      Author: Owner
 */

#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <fstream>
#include <cstring>
#include <sstream>
#include <cstdlib>
#include <set>
#include "insultgenerator_14cs76.h"

using namespace std;

const string FileException::what() {
	return "Error cannot open file. ";
}

const string NumInsultsOutOfBounds::what() {
	return "Error the number of insults is out of bounds. Choose between 1-10,000 ";
}

int InsultGenerator::random(int n) { //Helper function used to generate a random number from 0-n. n is the input
	return (rand() % n);
}

//initialize() method should load all the source phrases from the InsultsSource.txt file into the attributes.
void InsultGenerator::initialize() {
	ifstream readFile;
	srand(time(NULL)); //used for generating random numbers
	readFile.open("InsultsSource.txt", ios_base::in);

	if (readFile.is_open()) {
		string txtFromFile = "";
		string insult = "";
		stringstream ss;
		int i = 1;

		while (getline(readFile, txtFromFile)) {
			ss.str("");
			ss.clear();
			ss << txtFromFile;

			while (getline(ss, insult, '	')) { //tab deliminated

				if (i == 1) {
					insultCol1.push_back(insult);
					i = 2;
				} else if (i == 2) {
					insultCol2.push_back(insult);
					i = 3;
				} else if (i == 3) {
					insultCol3.push_back(insult);
					i = 1;
				}

			}

		}

	} else {
		throw FileException(); //throw error if you cannot open file
	}

	readFile.close();

}

// talkToMe() returns a single insult, generated at random.
string InsultGenerator::talkToMe() {

	string insult_string = "";

	insult_string = insultCol1[random(insultCol1.size())] + " "
			+ insultCol2[random(insultCol2.size())] + " "
			+ insultCol3[random(insultCol3.size())];

	return insult_string;
}

vector<string> InsultGenerator::generate(int size) {
	set<string> unique_insults;
	vector<string> final_insults;
	string insult = "";
	if (size < 1 || size > 10000) {
		throw NumInsultsOutOfBounds();
	} else {
		for (int i = 0; i < size; i++) {

			do {

				insult = talkToMe();

			} while (unique_insults.count(insult) == 1); // count function O(log(n))

			unique_insults.insert(insult);//Total complexity of doing n comparisons in log(n) time = O(nlog(n))
		}

		final_insults.assign(unique_insults.begin(), unique_insults.end()); //transfer set to a vector
		return final_insults;

	}

}

void InsultGenerator::generateAndSave(string filename, int num_generate) {
	ofstream outFile(filename);
	vector<string> insults;
	if (num_generate > 10000 || num_generate < 1) {
		throw NumInsultsOutOfBounds();
	}

	else {
		insults = generate(num_generate);

		for (vector<string>::iterator it = insults.begin(); it != insults.end(); //iterate and print to file
				++it) {
			outFile << *it;
			outFile << "\n";
		}
	}
}

