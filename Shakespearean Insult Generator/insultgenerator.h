/*
 * insultgenerator_14cs76.h
 *
 *  Created on: Sep 10, 2018
 *      Author: Owner
 */

#ifndef INSULTGENERATOR_14CS76_H_
#define INSULTGENERATOR_14CS76_H_

#include <time.h>
#include <fstream>
#include <algorithm>
#include <set>
#include <cstdlib>
#include <string.h>
#include <stdio.h>

using namespace std;

class InsultGenerator {
public:
	void initialize();
	string talkToMe();
	vector<string> generate(const int n);
	void generateAndSave(const string s, const int n);
private:
	int random(int n);
	vector<string> insultCol1, insultCol2, insultCol3;
	set<string> insults;
};

class FileException{
public:
	const string what();

};

class NumInsultsOutOfBounds{
public:
	const string what();

};


#endif /* INSULTGENERATOR_14CS76_H_ */
