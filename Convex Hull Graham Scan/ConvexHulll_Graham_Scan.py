import scipy as sc
import pprint
import matplotlib.pyplot as plt
import math




#implemneteding a stack
class Stack:
     def __init__(self):
         self.items = []

     def isEmpty(self):
         return self.items == []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop()

     def peek(self):
         return self.items[len(self.items)-1]

     def peek_second(self):
         return self.items[len(self.items)-2]

     def size(self):
         return len(self.items)

     def get_list(self):
         return self.items

def plot_2d_array(points, des,title):
    global figure
    x = []
    y = []
    print(des)
    for a in range(len(points)):
        x.append(points[a][0])
        y.append(points[a][1])

    plt.figure(figure)
    plt.title(title)
    plt.xlabel('X Axis')
    plt.ylabel('Y Axis')
    plt.plot(x, y, des)
   





def orientation(p,q,r):
    return (q[0] - p[0]) * (r[1] - p[1]) - (r[0] - p[0]) * (q[1] - p[1])


def convex_hull(points,type = ' '):
    # print("Intial Test Points " + type +" " +str(len(points)))
    # print(points)
    if len(points) < 3:
        #print("convex hull cannot be made")
        return points


    def polar_compare(p):
        x = p[0] - miny_point[0]
        y = p[1] - miny_point[1]
        return math.atan2(y,x) #returns in radians angle from positive x axis

    def distance(p):
        x = p[0] - miny_point[0]
        y = p[1] - miny_point[1]
        dist = math.sqrt(x**2 + y**2)
        return dist


    #find the minimum Y point tie going to the furthest left
    miny_point = min(points, key = lambda x:(x[1],x[0]))
    minY_index = points.index(miny_point)

    #swaping min points to front of list
    points[0],points[minY_index] = points[minY_index], points[0]

    #sorting by distance min first
    sorted_distance = sorted(points, key = distance)

    #sorting based on polar coordinates
    sorted_polar_angle = sorted(sorted_distance, key = polar_compare)

    ch = Stack()
    ch.push(sorted_polar_angle[0])
    ch.push(sorted_polar_angle[1])
    ch.push(sorted_polar_angle[2])

    for i in range (3, len(points)):

        while(orientation(ch.peek_second(),ch.peek(),sorted_polar_angle[i])<=0):
            ch.pop()
        ch.push(sorted_polar_angle[i])
    # print("Convex Hull")
    # print(ch.get_list())
    # print("Ratio")
    # print(str(len(ch.get_list())/len(points)))
    return ch.get_list()







######################################################################
## Main

#######################################################################

for i in [10,20,40,80,100,200,400,600,800,1000,2000,4000,6000,8000,10000]:
    num_points =  i

    sc.random.seed(12)
    #create a list of random points
    uniform_points = []
    normal_points = []
    ratio_normal = []
    ratio_uniform   = []
    size = num_points
    for a in range(num_points):
        normal_points.append((sc.random.normal(0, 0.4), sc.random.normal(0, 0.4)))
        uniform_points.append((sc.random.uniform(-1, 1), sc.random.uniform(-1, 1)))




    if(i ==100 ):
        figure =1
        plot_2d_array(uniform_points, 'ro', 'Uniform')
        uniform_points = convex_hull(uniform_points)

        plot_2d_array(uniform_points, 'bo', 'Uniform')
       

        figure =2

        plot_2d_array(normal_points, 'ro', 'NORMAL')
        normal_points = convex_hull(normal_points)
        plot_2d_array(normal_points, 'bo', 'NORMAL')

    normal_points =convex_hull(normal_points)
    uniform_points = convex_hull(uniform_points)
    ratio_normal.append(len(normal_points)/size)

    ratio_uniform.append(len(uniform_points)/size)



final_uniform= sum(ratio_uniform)/len(ratio_uniform)
final_normal  = sum(ratio_normal)/len(ratio_normal)

print("For sizes 10,20,40,80,100,200,400,600,800,1000,2000,4000,6000,8000,10000 the ratio for uniform and normal " )
print("uniform ratio of points on convex hull " + str(final_uniform))
print("normal ratio of points on convex hull " + str(final_normal))

###############################################################
##
# def ch_intersection(arr1,arr2):
#
#
#     def calc_mean(points):
#         x = []
#         y = []
#         fin = []
#
#         for a in range(len(points)):
#             x.append(points[a][0])
#             y.append(points[a][1])
#         return [sum(x)/len(x),sum(y)/len(y)]
#
#     def radius_calc(points, cx,cy):
#
#         max_rad =0
#         for i in range(len(points),):
#             x = points[i][0] - cx
#             y = points[i][1] - cy
#             rad = math.sqrt(x ** 2 + y ** 2)
#             if rad>max_rad:
#                 max_rad=rad
#         return max_rad
#
#     def distance(x1,y1,x2,y2):
#         return math.sqrt((x2-x1)**2 + (y2-y1)**2)
#
#     arr1 = convex_hull(arr1)
#     mean_1 = calc_mean(arr1)
#     rad_1 = radius_calc(arr1,mean_1[0],mean_1[1])
#
#
#
#     arr2 = convex_hull(arr2)
#     mean_2 = calc_mean(arr2)
#     rad_2 = radius_calc(arr2, mean_2[0], mean_2[1])
#
#     center_diff = distance(mean_1[0],mean_1[1],mean_2[0],mean_2[1])
#     if center_diff > (rad_1+rad_2):
#         print(" does not intersect ")
#     print("intersects")
#
#
#
#     c_1 = plt.Circle((mean_1[0], mean_1[1]), radius=rad_1, color='b', fill=False, clip_on =False)
#
#     c_2 = plt.Circle((mean_2[0], mean_2[1]), radius=rad_2, color='r', fill=False, clip_on =False)
#
#     plt.gcf().gca().add_artist(c_1)
#     plt.gcf().gca().add_artist(c_2)
#
#
#
#
# neg_points=[[-1,1],[-2,2],[-4,4],[-1,2],[-3,1],[-3,3]]
# plot_2d_array(neg_points, 'or','Intersecting hulls' )
#
# pos_points =[[0,3],[1,1],[2,2],[4,4],[0,0],[1,2],[3,1],[3,3]]
# plot_2d_array(pos_points, 'ob','Intersecting hulls' )
#
# ch_intersection(pos_points,neg_points)

plt.show()